# Palvelintekniikat, lopputyö
- Anni Karja, 2000639
- Joonas Lehtikangas, 2000206
- Antti Päivärinta 2000124,
- Viivi Tuomi, 2000511

Please run these commands in both directories (backend-1 and backend-2) for project to work! 
- npm i
- npm run build
- npm start

## API Description
This API is for recording pets. Pet consist of two fields: name and species.
Please give body parameters as JSON.
Server is localhost:3001

## Paths
### GET / 
### GET /pets
Returns all recorded pets.
Query parameters: None
Body parameters: None
Response codes:
- 200: OK

---
### GET /pets/{id}
Returns recorded pet with the given id.
Query parameters: 
- id: number

Body parameters: none

Response codes:
- 200: OK
- 404: Error, resource with id not found!
- 500: Error reading database!

---
### POST /pets
Record a new pet with given inputs. 
Query parameters: none
Body parameters: 
- name: string (required)
- species: string (required)

Example:

    {
        "name": "Kitty",
        "species": "Cat"
    }

Response codes:
- 201: Pet created
- 400: Error, required input missing!
- 500: Error reading database!
- 500: Error writing to database!

---
### PUT /pets/{id}
Update a recorded pet with new inputs. All or just one field can be updated.
Query parameters: 
- id
  
Body parameters: 
- name: string (optional)
- species: string (optional)

Example:

    {
        "name": "Garfield",
    }

Response codes:
- 200: OK
- 400: Error, required input missing!
- 404: Error, resource with id not found!
- 500: Error reading database!
- 500: Error writing to database!

---
### DELETE /pets/{id}
Deletes recorded pet with the given id.
Query parameters: 
- id: number

Body parameters: none

Response codes:
- 200: OK
- 404: Error, resource with id not found!
- 500: Error reading database!
- 500: Error writing to database!