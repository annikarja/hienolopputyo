"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pet = void 0;
class Pet {
    constructor(id, name, species) {
        this.id = id;
        this.name = name;
        this.species = species;
    }
}
exports.Pet = Pet;
