"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const dotenv = __importStar(require("dotenv"));
const fs = __importStar(require("fs"));
const dbReadError = 'Error reading database!';
const dbWriteError = 'Error writing to database!';
const notFoundError = (id) => {
    return `Error, resource with id ${id} not found!`;
};
const inputMissingError = () => {
    return `Error, required input missing!`;
};
dotenv.config();
const app = express();
app.use(cors({
    origin: `${process.env.URL}${process.env.SOURCE}`,
    methods: ['GET', 'POST', 'PUT', 'DELETE']
}));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.get('/', (req, res) => {
    fs.readFile(process.env.DB, 'utf8', (err, data) => {
        if (err)
            return res.status(500).json({ error: dbReadError });
        if (data) {
            return res.status(200).json(JSON.parse(data));
        }
        return res.status(200).send();
    });
});
app.get('/pets', (req, res) => {
    fs.readFile(process.env.DB, 'utf8', (err, data) => {
        if (err)
            return res.status(500).json({ error: dbReadError });
        if (data) {
            return res.status(200).json(JSON.parse(data));
        }
        return res.status(200).send();
    });
});
app.get('/pets/:id', (req, res) => {
    const id = Number(req.params.id);
    fs.readFile(process.env.DB, 'utf8', (err, data) => {
        if (err)
            return res.status(500).json({ error: dbReadError });
        if (data) {
            const pets = JSON.parse(data);
            const pet = pets.find((s) => { return s.id === id; });
            if (pet) {
                return res.status(200).json(pet);
            }
            return res.status(404).json({ error: notFoundError(id) });
        }
        return res.status(404).json({ error: notFoundError(id) });
    });
});
app.post('/pets', (req, res) => {
    const body = req.body;
    let newPet;
    if (!body.name || !body.species) {
        return res.status(400).json({ error: inputMissingError() });
    }
    let data;
    try {
        data = JSON.parse(fs.readFileSync(process.env.DB, 'utf8'));
    }
    catch (_a) {
        return res.status(500).json({ error: dbWriteError });
    }
    if (data && data.length) {
        const maxId = Math.max(...data.map((row) => row.id));
        body.id = maxId + 1;
    }
    else {
        body.id = 0;
    }
    newPet = body;
    data.push(newPet);
    try {
        fs.writeFileSync(process.env.DB, JSON.stringify(data), 'utf8');
    }
    catch (err) {
        return res.status(500).json({ error: dbWriteError });
    }
    return res.status(201).json(newPet);
});
app.put('/pets/:id', (req, res) => {
    const id = Number(req.params.id);
    const body = req.body;
    let data;
    try {
        data = JSON.parse(fs.readFileSync(process.env.DB, 'utf8'));
    }
    catch (_a) {
        return res.status(500).json({ error: dbWriteError });
    }
    const petIndex = data.findIndex((row) => { return row.id === id; });
    if (petIndex < 0) {
        return res.status(404).json({ error: notFoundError(id) });
    }
    if (body.name)
        data[petIndex].name = body.name;
    if (body.species)
        data[petIndex].species = body.species;
    const updatedPet = data[petIndex];
    try {
        fs.writeFileSync(process.env.DB, JSON.stringify(data), 'utf8');
    }
    catch (err) {
        return res.status(500).json({ error: dbWriteError });
    }
    return res.status(200).json(updatedPet);
});
app.delete('/pets/:id', (req, res) => {
    const id = Number(req.params.id);
    let data = JSON.parse(fs.readFileSync(process.env.DB, 'utf8'));
    const petIndex = data.findIndex((row) => { return row.id === id; });
    if (petIndex < 0) {
        return res.status(404).json({ error: notFoundError(id) });
    }
    const deletedPet = data[petIndex];
    data = data.filter((pet) => { return pet.id !== id; });
    try {
        fs.writeFileSync(process.env.DB, JSON.stringify(data), 'utf8');
    }
    catch (err) {
        return res.status(500).json({ error: dbWriteError });
    }
    return res.status(200).json(deletedPet);
});
app.listen(process.env.PORT, () => {
    console.log(`Server listening to port ${process.env.PORT}`);
});
