export class Pet {
    id: number;
    name: string; 
    species: string;

    constructor(id: number, name: string, species: string) {
        this.id = id;
        this.name = name;
        this.species = species;
    }
}