import express = require('express');
import cors = require('cors');
import * as dotenv from 'dotenv';
import * as fs from 'fs';

import { Pet } from './Pet';

const dbReadError: string = 'Error reading database!';
const dbWriteError: string = 'Error writing to database!';
const notFoundError = (id: number) => {
    return `Error, resource with id ${id} not found!`;
}
const inputMissingError = () => {
    return `Error, required input missing!`;
}

dotenv.config();
const app = express();

app.use(cors({
    origin: `${process.env.URL as string}${process.env.SOURCE}`,
    methods: ['GET', 'POST', 'PUT', 'DELETE']
}));

app.use(express.urlencoded({extended:true}));
app.use(express.json());


app.get('/', (req, res) => {
    fs.readFile(process.env.DB as string, 'utf8', (err: any, data: any) => {
        if (err) return res.status(500).json({ error: dbReadError })

        if (data) {
            
            return res.status(200).json(JSON.parse(data));
        }
        return res.status(200).send();
    });
});

app.get('/pets', (req, res) => {
    fs.readFile(process.env.DB as string, 'utf8', (err: any, data: any) => {
        if (err) return res.status(500).json({ error: dbReadError })

        if (data) {
            return res.status(200).json(JSON.parse(data));
        }
        return res.status(200).send();
    });
});

app.get('/pets/:id', (req, res) => {
    const id = Number(req.params.id);
    fs.readFile(process.env.DB as string, 'utf8', (err: any, data: any) => {
        if (err) return res.status(500).json({ error: dbReadError })

        if (data) {
            const pets: Pet[] = JSON.parse(data);
            const pet: Pet | undefined = pets.find((s: Pet) => { return s.id === id });
            if (pet) {
                return res.status(200).json(pet)
            }

            return res.status(404).json({ error: notFoundError(id) });
        }
        return res.status(404).json({ error: notFoundError(id) });
    });
})

app.post('/pets', (req, res) => {
    const body = req.body;
    let newPet: Pet;

    if (!body.name || !body.species) {
        return res.status(400).json({ error: inputMissingError() })
    }

    let data: Pet[];
    try {
        data = JSON.parse(fs.readFileSync(process.env.DB as string, 'utf8'));
    } catch {
        return res.status(500).json({ error: dbWriteError });
    }

    if (data && data.length) {
        const maxId = Math.max(...data.map((row: Pet) => row.id));
        body.id = maxId + 1;
    } else {
        body.id = 0;
    }
    
    newPet = body;
    data.push(newPet);

    try {
        fs.writeFileSync(process.env.DB as string, JSON.stringify(data), 'utf8');
    } catch (err: any) {
        return res.status(500).json({ error: dbWriteError });
    }

    return res.status(201).json(newPet);
})

app.put('/pets/:id', (req, res) => {
    const id = Number(req.params.id);
    const body = req.body;

    let data: Pet[];
    try {
        data = JSON.parse(fs.readFileSync(process.env.DB as string, 'utf8'));
    } catch {
        return res.status(500).json({ error: dbWriteError });
    }

    const petIndex: number = data.findIndex((row: any) => { return row.id === id });
    if (petIndex < 0) {
        return res.status(404).json({ error: notFoundError(id) });
    }

    if (body.name) data[petIndex].name = body.name;
    if (body.species) data[petIndex].species = body.species;
    const updatedPet = data[petIndex];

    try {
        fs.writeFileSync(process.env.DB as string, JSON.stringify(data), 'utf8');
    }
    catch (err: any) {
        return res.status(500).json({ error: dbWriteError });
    }

    return res.status(200).json(updatedPet);
})

app.delete('/pets/:id', (req, res) => {
    const id = Number(req.params.id);

    let data: Pet[] = JSON.parse(fs.readFileSync(process.env.DB as string, 'utf8'));
    const petIndex: number = data.findIndex((row: any) => { return row.id === id });
    if (petIndex < 0) {
        return res.status(404).json({ error: notFoundError(id) });
    }

    const deletedPet = data[petIndex];
    data = data.filter((pet: Pet) => { return pet.id !== id });

    try {
        fs.writeFileSync(process.env.DB as string, JSON.stringify(data), 'utf8');
    }
    catch (err: any) {
        return res.status(500).json({ error: dbWriteError });
    }

    return res.status(200).json(deletedPet);
})

app.listen(process.env.PORT as string, () => {
    console.log(`Server listening to port ${process.env.PORT}`)
})