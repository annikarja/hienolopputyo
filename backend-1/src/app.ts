import fetch from 'node-fetch';
import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';

dotenv.config();

const app = express();

app.use(cors({
    origin: "*",
    methods: ["GET","POST","PUT","DELETE"]
}));

app.use(express.urlencoded({extended:true}));
app.use(express.json());

app.get('/', (req: express.Request, res: express.Response) => {
    fetch(`http://${process.env.SERVER2}:${process.env.PORT2}`)
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
})

app.get('/pets', (req: express.Request, res: express.Response) => {
    fetch(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}`)
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
})

app.get('/pets/:id', (req: express.Request, res: express.Response) => {
    let id: number = Number(req.params.id);
    fetch(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}/${id}`)
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
})

app.post("/pets", (req: express.Request, res: express.Response) => {
    fetch(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}`, {
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: { 'Content-Type': 'application/json' }
    })
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
})

app.put("/pets/:id", (req: express.Request, res: express.Response) => {
    let id: number = Number(req.params.id);
    fetch(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}/${id}`, {
        method: 'PUT',
        body: JSON.stringify(req.body),
        headers: { 'Content-Type': 'application/json' }
    })
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
})

app.delete("/pets/:id", (req: express.Request, res: express.Response) => {
    console.log(req.body)
    let id: number = Number(req.params.id);
    fetch(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}/${id}`, {
        method: 'DELETE'
    })
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
})

const PORT = process.env.PORT || 3002
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})