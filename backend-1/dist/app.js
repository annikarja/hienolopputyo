"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = __importDefault(require("node-fetch"));
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
dotenv_1.default.config();
const app = (0, express_1.default)();
app.use((0, cors_1.default)({
    origin: "*",
    methods: ["GET", "POST", "PUT", "DELETE"]
}));
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
app.get('/', (req, res) => {
    (0, node_fetch_1.default)(`http://${process.env.SERVER2}:${process.env.PORT2}`)
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
});
app.get('/pets', (req, res) => {
    (0, node_fetch_1.default)(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}`)
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
});
app.get('/pets/:id', (req, res) => {
    let id = Number(req.params.id);
    (0, node_fetch_1.default)(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}/${id}`)
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
});
app.post("/pets", (req, res) => {
    (0, node_fetch_1.default)(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}`, {
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: { 'Content-Type': 'application/json' }
    })
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
});
app.put("/pets/:id", (req, res) => {
    let id = Number(req.params.id);
    (0, node_fetch_1.default)(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}/${id}`, {
        method: 'PUT',
        body: JSON.stringify(req.body),
        headers: { 'Content-Type': 'application/json' }
    })
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
});
app.delete("/pets/:id", (req, res) => {
    console.log(req.body);
    let id = Number(req.params.id);
    (0, node_fetch_1.default)(`http://${process.env.SERVER2}:${process.env.PORT2}/${process.env.PETS}/${id}`, {
        method: 'DELETE'
    })
        .then(res => res.json())
        .then(json => res.send(json))
        .catch(err => console.log(err));
});
const PORT = process.env.PORT || 3002;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
